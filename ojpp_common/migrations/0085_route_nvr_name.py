# Generated by Django 4.1.7 on 2023-03-24 20:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ojpp_common', '0084_alter_timetableexception_marprom_id_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='route',
            name='nvr_name',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
