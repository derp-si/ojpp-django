# Generated by Django 4.1.6 on 2023-02-10 19:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ojpp_common', '0062_remove_operator_vehicle_count_remove_route_geometry_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='stoplocation',
            name='photo',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
    ]
