# Generated by Django 4.1.7 on 2023-04-22 21:01

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("ojpp_common", "0086_operator_color"),
    ]

    operations = [
        migrations.AddField(
            model_name="operator",
            name="short_name",
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
