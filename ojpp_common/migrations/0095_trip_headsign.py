# Generated by Django 4.2 on 2024-05-12 12:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ojpp_common', '0094_route_route_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='trip',
            name='headsign',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
    ]
